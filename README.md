# Kotlin Movies

Kotlin Movies is a sample project to showcase different architectural approaches & libraries to develop Android apps.

## Samples
|     Branch    | Description |
| ------------- | ------------- |
| [master](https://gitlab.com/pablucu/KotlinMovies/-/tree/master) | The base for the rest of the branches. Uses Kotlin, coroutines, [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) and well known classes such as SQLite for local database and Retrofit for network requests. |
| [viewbinding](https://gitlab.com/pablucu/KotlinMovies/-/tree/viewbinding) | Uses [View Binding](https://developer.android.com/topic/libraries/view-binding) to replace `findViewById`.
| [mvvm](https://gitlab.com/pablucu/KotlinMovies/-/tree/mvvm) | Uses [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) to implement [MVVM](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel) pattern.
| [room](https://gitlab.com/pablucu/KotlinMovies/-/tree/room) | Uses [Room](https://developer.android.com/training/data-storage/room)  to handle the local SQLite database.


### MVP Samples (deprecated)
|     Branch    | Description |
| ------------- | ------------- |
| [mvp](https://gitlab.com/pablucu/KotlinMovies/-/tree/mvp) | Simple Android app using [MVP](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter) pattern.|
| [mvp_coroutines](https://gitlab.com/pablucu/KotlinMovies/-/tree/mvp_coroutines) | Uses [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html) to handle background operations. |
| [mvp_dagger2](https://gitlab.com/pablucu/KotlinMovies/-/tree/mvp_dagger2) | Uses [Dagger 2](https://dagger.dev/dev-guide/) for dependency injection and removes the two flavors. |

## Languages & libraries

* [Kotlin](https://kotlinlang.org/)
* [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html)
* [Retrofit](https://square.github.io/retrofit/)
* [OkHttp](https://square.github.io/okhttp/)
* [Gson](https://github.com/google/gson)
* [Espresso](https://developer.android.com/training/testing/espresso)
* [Mockito](https://site.mockito.org/)

## Requirements

* [Android Studio SDK](https://developer.android.com/studio/)
* JDK 1.8

## Testing Notes
There are two flavors:
* 'prod' to run the real app
* 'mock' to make testing easier

## Opening a sample in Android Studio

Clone the repository:
```
https://gitlab.com/pablucu/KotlinMovies.git
```