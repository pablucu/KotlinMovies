package demo.petproject.kotlinmovies.data.local

import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.domain.entities.Result
import org.junit.Assert
import java.lang.Exception

class LocalDaoImplTestHelper {

    companion object {
        val MOVIE_1 = Movie(1L, "Movie One", "30/03/1995", "summary1", 1000, 5.5, "movie1_poster", "movie1_backdrop", 1)
        val MOVIE_1_UPDATED = Movie(1L, "New Movie One", "02/01/2000", "summary100", 100, 7.8, "movie1_poster2", "movie1_backdrop2", 3)
        val MOVIE_2 = Movie(2L, "Movie Two", "28/02/1950", "summary2", 2000, 7.5, "movie2_poster", "movie2_backdrop", 2)

        fun assertIsError(result: Result<Any>, expectedException: Exception) {
            Assert.assertTrue(result is Result.Error)
            result as Result.Error
            Assert.assertEquals(result.exception.javaClass, expectedException.javaClass)
            Assert.assertEquals(result.exception.message, expectedException.message)
        }

        fun assertIsMovie1(movie: Movie) {
            assertMovies(1L, "Movie One", "30/03/1995", "summary1", 1000, 5.5, "movie1_poster", "movie1_backdrop", 1, movie)
        }

        fun assertIsMovie1Updated(movie: Movie) {
            assertMovies(1L, "New Movie One", "02/01/2000", "summary100", 100, 7.8, "movie1_poster2", "movie1_backdrop2", 3, movie)
        }

        fun assertIsMovie2(movie: Movie) {
            assertMovies(2L, "Movie Two", "28/02/1950", "summary2", 2000, 7.5, "movie2_poster", "movie2_backdrop", 2, movie)

        }

        fun assertIsMovie2Updated(movie: Movie) {
            assertMovies(2L, "New Movie Two", "02/02/2002", "summary202", 202, 7.2, "movie2_poster2", "movie2_backdrop2", 4, movie)
        }

        private fun assertMovies(id: Long?,
                                 title: String,
                                 localDate: String,
                                 overview: String,
                                 votes: Int?,
                                 vote_average: Double?,
                                 poster: String?,
                                 backdrop: String?,
                                 position: Int?,
                                 movie: Movie) {
            Assert.assertEquals(id, movie.id)
            Assert.assertEquals(title, movie.title)
            Assert.assertEquals(localDate, movie.releaseDate)
            Assert.assertEquals(overview, movie.overview)
            Assert.assertEquals(votes, movie.voteCount)
            Assert.assertEquals(vote_average, movie.voteAverage)
            Assert.assertEquals(poster, movie.poster)
            Assert.assertEquals(backdrop, movie.backdrop)
            Assert.assertEquals(position, movie.position)
        }
    }
}
