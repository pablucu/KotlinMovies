package demo.petproject.kotlinmovies.data.local.sqlite

import androidx.test.platform.app.InstrumentationRegistry
import demo.petproject.kotlinmovies.data.local.LocalDao
import demo.petproject.kotlinmovies.data.local.LocalDaoImplTestHelper
import demo.petproject.kotlinmovies.data.local.LocalDaoImplTestHelper.Companion.MOVIE_1
import demo.petproject.kotlinmovies.data.local.LocalDaoImplTestHelper.Companion.MOVIE_1_UPDATED
import demo.petproject.kotlinmovies.data.local.LocalDaoImplTestHelper.Companion.MOVIE_2
import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.exception.NoMovieException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class LocalDaoImplTest {
    private lateinit var db: LocalDao

    @Before
    fun setUp() {
        val instrumentation = InstrumentationRegistry.getInstrumentation()
        db = LocalDaoImpl(instrumentation.targetContext.applicationContext)
    }

    @After
    fun tearDown() = runTest {
        db.deleteMovies()
    }

    @Test
    fun test_emptyDatabase_and_getMovie() = runTest {
        val retrievedMovie = db.getMovie(MOVIE_1.id)

        LocalDaoImplTestHelper.assertIsError(retrievedMovie, NoMovieException("Movie not found!"))
    }

    @Test
    fun test_saveMovie_and_getMovie() = runTest {
        db.saveMovie(MOVIE_1)

        val retrievedMovie = db.getMovie(MOVIE_1.id) as Result.Success<Movie>

        LocalDaoImplTestHelper.assertIsMovie1(retrievedMovie.data)
    }

    @Test
    fun test_UpdateMovie() = runTest {
        db.saveMovie(MOVIE_1)

        val retrievedMovie = db.getMovie(MOVIE_1.id) as Result.Success<Movie>

        retrievedMovie.data.title = "New Movie One"
        retrievedMovie.data.releaseDate = "02/01/2000"
        retrievedMovie.data.overview = "summary100"
        retrievedMovie.data.voteCount = 100
        retrievedMovie.data.voteAverage = 7.8
        retrievedMovie.data.poster = "movie1_poster2"
        retrievedMovie.data.backdrop = "movie1_backdrop2"
        retrievedMovie.data.position = 3

        db.updateMovie(retrievedMovie.data)

        val updatedMovie = db.getMovie(retrievedMovie.data.id) as Result.Success<Movie>
        LocalDaoImplTestHelper.assertIsMovie1Updated(updatedMovie.data)
    }

    @Test
    fun test_DeleteMovie() = runTest {
        db.saveMovie(MOVIE_1)

        db.deleteMovie(MOVIE_1.id)

        val retrievedMovie = db.getMovie(MOVIE_1.id)
        LocalDaoImplTestHelper.assertIsError(retrievedMovie, NoMovieException("Movie not found!"))
    }

    @Test
    fun test_SaveMovieWithDuplicatedId() = runTest {
        db.saveMovie(MOVIE_1)
        db.saveMovie(MOVIE_1_UPDATED)

        // Check there's only one movie saved in the database
        val moviesResult = db.getMovies() as Result.Success<List<Movie>>

        Assert.assertEquals(1, moviesResult.data.size)

        // Check the saved movie has the latest information
        val updatedMovieResult = db.getMovie(MOVIE_1.id) as Result.Success<Movie>
        LocalDaoImplTestHelper.assertIsMovie1Updated(updatedMovieResult.data)
    }

    @Test
    fun test_SaveMovies() = runTest {

        db.saveMovies(listOf(MOVIE_1, MOVIE_2))

        // Check there are two movies saved in the database
        val moviesResult = db.getMovies() as Result.Success<List<Movie>>
        Assert.assertEquals(2, moviesResult.data.size)

        // Check the saved movies
        val movie1 = db.getMovie(MOVIE_1.id) as Result.Success<Movie>
        LocalDaoImplTestHelper.assertIsMovie1(movie1.data)

        val movie2 = db.getMovie(MOVIE_2.id) as Result.Success<Movie>
        LocalDaoImplTestHelper.assertIsMovie2(movie2.data)
    }

    @Test
    fun test_GetMovies() = runTest {
        db.saveMovies(listOf(MOVIE_1, MOVIE_2))

        val moviesResult = db.getMovies() as Result.Success<List<Movie>>

        Assert.assertEquals(2, moviesResult.data.size)
        LocalDaoImplTestHelper.assertIsMovie1(moviesResult.data[0])
        LocalDaoImplTestHelper.assertIsMovie2(moviesResult.data[1])
    }

    @Test
    fun test_GetMoviesIsSortedByPosition() = runTest {
        db.saveMovies(listOf(MOVIE_1_UPDATED, MOVIE_2))

        val movies = db.getMovies() as Result.Success<List<Movie>>

        Assert.assertEquals(2, movies.data.size)
        LocalDaoImplTestHelper.assertIsMovie2(movies.data[0])
        LocalDaoImplTestHelper.assertIsMovie1Updated(movies.data[1])
    }

    @Test
    fun test_UpdateMovies() = runTest {
        db.saveMovies(listOf(MOVIE_1, MOVIE_2))

        var movies = db.getMovies() as Result.Success<List<Movie>>

        val updatedMovie = movies.data[0]
        updatedMovie.title = "New Movie One"
        updatedMovie.releaseDate = "02/01/2000"
        updatedMovie.overview = "summary100"
        updatedMovie.voteCount = 100
        updatedMovie.voteAverage = 7.8
        updatedMovie.poster = "movie1_poster2"
        updatedMovie.backdrop = "movie1_backdrop2"
        updatedMovie.position = 3

        val updatedMovie2 = movies.data[1]
        updatedMovie2.title = "New Movie Two"
        updatedMovie2.releaseDate = "02/02/2002"
        updatedMovie2.overview = "summary202"
        updatedMovie2.voteCount = 202
        updatedMovie2.voteAverage = 7.2
        updatedMovie2.poster = "movie2_poster2"
        updatedMovie2.backdrop = "movie2_backdrop2"
        updatedMovie2.position = 4

        db.updateMovies(listOf(updatedMovie, updatedMovie2))

        movies = db.getMovies() as Result.Success<List<Movie>>
        LocalDaoImplTestHelper.assertIsMovie1Updated(movies.data[0])
        LocalDaoImplTestHelper.assertIsMovie2Updated(movies.data[1])
    }

    @Test
    fun test_DeleteAllMovies() = runTest {
        db.saveMovies(listOf(MOVIE_1, MOVIE_2))
        var movies = db.getMovies() as Result.Success<List<Movie>>
        Assert.assertEquals(2, movies.data.size)

        db.deleteMovies()

        movies = db.getMovies() as Result.Success<List<Movie>>
        Assert.assertEquals(0, movies.data.size)
    }

    @Test
    fun test_SaveMoviesWithDuplicatedId() = runTest {

        db.saveMovies(listOf(MOVIE_1, MOVIE_2, MOVIE_1_UPDATED))

        // Check there are two movies saved in the database
        val movies = db.getMovies() as Result.Success<List<Movie>>
        Assert.assertEquals(2, movies.data.size)

        // Check the saved movies
        val movie1 = db.getMovie(MOVIE_1.id) as Result.Success<Movie>
        LocalDaoImplTestHelper.assertIsMovie1Updated(movie1.data)

        val movie2 = db.getMovie(MOVIE_2.id) as Result.Success<Movie>
        LocalDaoImplTestHelper.assertIsMovie2(movie2.data)
    }

}
