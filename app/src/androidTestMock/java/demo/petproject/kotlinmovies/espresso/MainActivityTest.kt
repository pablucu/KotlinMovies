package demo.petproject.kotlinmovies.espresso

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import demo.petproject.kotlinmovies.R
import demo.petproject.kotlinmovies.TestData
import demo.petproject.kotlinmovies.data.local.LocalDao
import demo.petproject.kotlinmovies.data.local.LocalDaoImplTestHelper.Companion.MOVIE_1
import demo.petproject.kotlinmovies.data.remote.RemoteDao
import demo.petproject.kotlinmovies.di.Factory
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.exception.NetworkConnectionException
import demo.petproject.kotlinmovies.ui.android.activity.MainActivity
import demo.petproject.kotlinmovies.ui.android.activity.MovieDetailsActivity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mockito
import org.mockito.Mockito.reset
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    private lateinit var remoteDao: RemoteDao

    private lateinit var localDao: LocalDao

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        remoteDao = Factory.getRemoteDao()
        localDao = Factory.getLocalDao()
    }

    @After
    fun tearDown() {
        Intents.release()
        reset(remoteDao)
        reset(localDao)
    }

    @Test
    fun test_Given_RemoteData_Then_DisplayRemoteData() {

        // Setup RemoteDAO to return a movie
        setupRemoteDataWithOneMovie()

        // Start activity
        ActivityScenario.launch(MainActivity::class.java)

        // Check the movie is displayed
        onView(withId(R.id.adapter_movie_list_name)).check(matches(withText("Movie One")))
    }

    @Test
    fun test_Given_NoRemoteData_And_LocalData_Then_DisplayLocalData() {

        // Setup RemoteDAO to return an error
        setupRemoteDataError()

        // Setup LocalData has movies
        setupLocalDataWithMovies()

        // Start activity
        ActivityScenario.launch(MainActivity::class.java)

        // Check the movie is displayed
        onView(withId(R.id.adapter_movie_list_name)).check(matches(withText("Movie Two")))
    }

    @Test
    fun test_Given_NoRemoteData_And_NoLocalData_Then_DisplayError() {

        // Setup RemoteDAO to return an error
        setupRemoteDataError()

        // Setup LocalData does not have movies
        setupLocalDataWithoutMovies()

        // Start activity
        ActivityScenario.launch(MainActivity::class.java)

        // Check the right error message is displayed
        val expected = InstrumentationRegistry.getInstrumentation().targetContext.resources.getString(R.string.error_loading_movies)
        onView(withId(R.id.error_text)).check(matches(withText(expected)))
    }

    @Test
    fun test_When_MovieIsClicked_Then_MovieDetailsAreDisplayed() {

        // Setup RemoteDAO to return a movie
        setupRemoteDataWithOneMovie()

        // Start activity
        ActivityScenario.launch(MainActivity::class.java)

        // When user clicks a movie
        onView(withId(R.id.adapter_movie_list_container)).perform(click())

        // Verify the movie details activity is started
        checkMovieDetailsActivityIsStarted(MOVIE_1.id)
    }

    private fun checkMovieDetailsActivityIsStarted(movieId: Long) {
        intended(hasComponent(MovieDetailsActivity::class.java.name))
        intended(hasExtra(MovieDetailsActivity.MOVIE_ID_EXTRA, movieId))
    }

    private fun setupRemoteDataWithOneMovie() = runTest {
        // Simulate RemoteDao successful response with sample remote data
        Mockito.`when`(remoteDao.getMovies(anyInt())).thenReturn(Result.Success(listOf(MOVIE_1)))
    }

    private fun setupRemoteDataError() = runTest {
        // Simulate RemoteDao error response
        Mockito.`when`(remoteDao.getMovies(anyInt())).thenReturn(Result.Error(NetworkConnectionException("Error downloading movies")))
    }

    private fun setupLocalDataWithMovies() = runTest {
        Mockito.`when`(localDao.getMovies()).thenReturn(Result.Success(listOf(TestData.MOVIE_2)))
    }

    private fun setupLocalDataWithoutMovies() = runTest {
        Mockito.`when`(localDao.getMovies()).thenReturn(Result.Success(listOf()))
    }

}
