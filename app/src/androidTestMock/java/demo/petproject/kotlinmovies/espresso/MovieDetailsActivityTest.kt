package demo.petproject.kotlinmovies.espresso

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import demo.petproject.kotlinmovies.R
import demo.petproject.kotlinmovies.TestData
import demo.petproject.kotlinmovies.data.local.LocalDao
import demo.petproject.kotlinmovies.di.Factory
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.exception.NoMovieException
import demo.petproject.kotlinmovies.ui.android.activity.MovieDetailsActivity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class MovieDetailsActivityTest {

    private lateinit var localDao: LocalDao

    private lateinit var intent: Intent

    @Rule
    @JvmField
    var movieDetailsActivity: ActivityTestRule<MovieDetailsActivity> = ActivityTestRule(MovieDetailsActivity::class.java)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        localDao = Factory.getLocalDao()
    }

    @After
    fun tearDown() {
        Mockito.reset(localDao)
    }

    @Test
    fun test_Given_NoMovieId_Then_DisplayError() {

        // Setup LocalData without movies
        setupLocalDataWithoutMovies(localDao)

        // Start activity without movie id
        movieDetailsActivity.launchActivity(Intent())

        // Check the right error message is displayed
        val expected = InstrumentationRegistry.getInstrumentation().targetContext.resources.getString(R.string.error_no_movie)
        onView(withId(R.id.error_text)).check(matches(withText(expected)))
    }

    @Test
    fun test_Given_UnknownMovieId_Then_DisplayError() {

        // Setup intent with unknown movie Id
        intent = Intent()
        intent.putExtra(MovieDetailsActivity.MOVIE_ID_EXTRA, 99L)

        // Setup LocalData without movies
        setupLocalDataWithoutMovies(localDao)

        // Start activity
        movieDetailsActivity.launchActivity(intent)

        // Check the right error message is displayed
        val expected = InstrumentationRegistry.getInstrumentation().targetContext.resources.getString(R.string.error_no_movie)
        onView(withId(R.id.error_text)).check(matches(withText(expected)))
    }

    @Test
    fun test_Given_MovieId_Then_DisplayMovieDetails() {

        // Setup intent with MOVIE_1 id
        intent = Intent()
        intent.putExtra(MovieDetailsActivity.MOVIE_ID_EXTRA, TestData.MOVIE_1.id)

        // Setup LocalData with movie 1
        setupLocalDataWithMovie1(localDao)

        // Start activity
        movieDetailsActivity.launchActivity(intent)

        // Verify the movie details are displayed
        checkMovieDetails()
    }

    private fun checkMovieDetails() {
        onView(withId(R.id.movie_title)).check(matches(withText(TestData.MOVIE_1.title)))
        onView(withId(R.id.movie_date)).check(matches(withText(TestData.MOVIE_1.releaseDate)))
        onView(withId(R.id.movie_vote_average)).check(matches(withText(TestData.MOVIE_1.voteAverage.toString())))
        val votesString = InstrumentationRegistry.getInstrumentation().targetContext.resources.getString(R.string.movie_votes, TestData.MOVIE_1.voteCount)
        onView(withId(R.id.movie_vote_count)).check(matches(withText(votesString)))
        onView(withId(R.id.movie_overview)).check(matches(withText(TestData.MOVIE_1.overview)))
    }

    private fun setupLocalDataWithoutMovies(localDao: LocalDao) = runTest {
        Mockito.`when`(localDao.getMovie(anyLong())).thenReturn(Result.Error(NoMovieException("Movie not found!")))
    }

    private fun setupLocalDataWithMovie1(localDao: LocalDao) = runTest {
        Mockito.`when`(localDao.getMovie(TestData.MOVIE_1.id)).thenReturn(Result.Success(TestData.MOVIE_1))
    }

}
