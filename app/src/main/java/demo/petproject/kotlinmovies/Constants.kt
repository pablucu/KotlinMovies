package demo.petproject.kotlinmovies

object Constants {

    // API
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val BASE_IMG_URL = "https://image.tmdb.org/t/p/w200"
    const val BASE_BACKDROP_URL = "https://image.tmdb.org/t/p/w300"

    // Dates
    const val DATE_FORMAT = "yyyy-MM-dd"

    // UI
    const val ROUNDING_RADIUS = 12

}