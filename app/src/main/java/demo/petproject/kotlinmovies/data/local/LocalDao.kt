package demo.petproject.kotlinmovies.data.local

import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.domain.entities.Result

interface LocalDao {

    // One Movie operations

    suspend fun saveMovie(newMovie: Movie)

    suspend fun getMovie(movieId: Long): Result<Movie>

    suspend fun updateMovie(movie: Movie)

    suspend fun deleteMovie(movieId: Long)

    // Many Movies operations

    suspend fun saveMovies(movies: List<Movie>)

    suspend fun getMovies(): Result<List<Movie>>

    suspend fun updateMovies(movies: List<Movie>)

    suspend fun deleteMovies()
}