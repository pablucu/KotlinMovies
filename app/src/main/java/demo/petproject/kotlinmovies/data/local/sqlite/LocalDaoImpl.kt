package demo.petproject.kotlinmovies.data.local.sqlite

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import demo.petproject.kotlinmovies.data.local.LocalDao
import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.exception.NoMovieException
import java.util.*

class LocalDaoImpl(context: Context) : LocalDao {

    private val dbHelper: SQLiteHelper = SQLiteHelper(context)

    override suspend fun saveMovie(newMovie: Movie) {

        val db = dbHelper.writableDatabase

        val values = ContentValues()
        values.put(SQLiteHelper.MOVIE_ID, newMovie.id)
        values.put(SQLiteHelper.TITLE, newMovie.title)
        values.put(SQLiteHelper.RELEASE_DATE, newMovie.releaseDate)
        values.put(SQLiteHelper.OVERVIEW, newMovie.overview)
        values.put(SQLiteHelper.VOTES, newMovie.voteCount)
        values.put(SQLiteHelper.VOTE_AVG, newMovie.voteAverage)
        values.put(SQLiteHelper.POSTER, newMovie.poster)
        values.put(SQLiteHelper.BACKDROP, newMovie.backdrop)
        values.put(SQLiteHelper.POSITION, newMovie.position)
        db.insertWithOnConflict(SQLiteHelper.TABLE_MOVIES, null, values, SQLiteDatabase.CONFLICT_REPLACE)
    }

    override suspend fun getMovie(movieId: Long): Result<Movie> {

        try {
            var movie: Movie? = null
            val db = dbHelper.readableDatabase

            val selection = SQLiteHelper.MOVIE_ID + " LIKE ?"
            val selectionArgs = arrayOf(movieId.toString())

            val cursor = db.query(SQLiteHelper.TABLE_MOVIES, SQLiteHelper.MOVIE_COLUMNS, selection, selectionArgs, null, null, null)

            if (cursor != null) {
                cursor.moveToFirst()
                if (!cursor.isAfterLast) {
                    movie = Movie()
                    movie.id = cursor.getLong(0)
                    movie.title = cursor.getString(1)
                    movie.releaseDate = cursor.getString(2)
                    movie.overview = cursor.getString(3)
                    movie.voteCount = cursor.getInt(4)
                    movie.voteAverage = cursor.getDouble(5)
                    movie.poster = cursor.getString(6)
                    movie.backdrop = cursor.getString(7)
                    movie.position = cursor.getInt(8)
                }
                cursor.close()
            }
            return if (movie != null) Result.Success(movie) else Result.Error(NoMovieException("Movie not found!"))
        } catch (e: Exception) {
            return Result.Error(e)
        }
    }

    override suspend fun updateMovie(movie: Movie) {

        val db = dbHelper.writableDatabase

        val values = ContentValues()
        values.put(SQLiteHelper.MOVIE_ID, movie.id)
        values.put(SQLiteHelper.TITLE, movie.title)
        values.put(SQLiteHelper.RELEASE_DATE, movie.releaseDate)
        values.put(SQLiteHelper.OVERVIEW, movie.overview)
        values.put(SQLiteHelper.VOTES, movie.voteCount)
        values.put(SQLiteHelper.VOTE_AVG, movie.voteAverage)
        values.put(SQLiteHelper.POSTER, movie.poster)
        values.put(SQLiteHelper.BACKDROP, movie.backdrop)
        values.put(SQLiteHelper.POSITION, movie.position)

        val selection = SQLiteHelper.MOVIE_ID + " LIKE ?"
        val selectionArgs = arrayOf(movie.id.toString())

        db.update(SQLiteHelper.TABLE_MOVIES, values, selection, selectionArgs)
    }

    override suspend fun deleteMovie(movieId: Long) {

        val db = dbHelper.writableDatabase

        val selection = SQLiteHelper.MOVIE_ID + " LIKE ?"
        val selectionArgs = arrayOf(movieId.toString())

        db.delete(SQLiteHelper.TABLE_MOVIES, selection, selectionArgs)
    }

    override suspend fun saveMovies(movies: List<Movie>) {
        val db = dbHelper.writableDatabase

        val sql = "insert or replace into " + SQLiteHelper.TABLE_MOVIES +
                "(" + SQLiteHelper.MOVIE_ID + "," +
                SQLiteHelper.TITLE + "," +
                SQLiteHelper.RELEASE_DATE + "," +
                SQLiteHelper.OVERVIEW + "," +
                SQLiteHelper.VOTES + "," +
                SQLiteHelper.VOTE_AVG + "," +
                SQLiteHelper.POSTER + "," +
                SQLiteHelper.BACKDROP + "," +
                SQLiteHelper.POSITION + ")" +
                "values (?, ?, ?, ?, ?, ?, ?, ?, ?);"

        db.beginTransaction()
        val stmt = db.compileStatement(sql)

        for (movie in movies) {
            stmt.bindLong(1, movie.id)
            stmt.bindString(2, movie.title)
            stmt.bindString(3, movie.releaseDate)
            stmt.bindString(4, movie.overview)
            stmt.bindLong(5, movie.voteCount.toLong())
            stmt.bindDouble(6, movie.voteAverage)
            stmt.bindString(7, movie.poster)
            stmt.bindString(8, movie.backdrop)
            stmt.bindLong(9, movie.position.toLong())

            stmt.execute()
            stmt.clearBindings()
        }

        db.setTransactionSuccessful()
        db.endTransaction()
    }

    override suspend fun getMovies(): Result<List<Movie>> {
        try {
            val db = dbHelper.readableDatabase

            val movies = ArrayList<Movie>()

            val cursor = db.query(SQLiteHelper.TABLE_MOVIES, SQLiteHelper.MOVIE_COLUMNS, null, null, null, null, SQLiteHelper.POSITION + " ASC")

            if (cursor != null) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    if (!cursor.isAfterLast) {
                        val movie = Movie()
                        movie.id = cursor.getLong(0)
                        movie.title = cursor.getString(1)
                        movie.releaseDate = cursor.getString(2)
                        movie.overview = cursor.getString(3)
                        movie.overview = cursor.getString(3)
                        movie.voteCount = cursor.getInt(4)
                        movie.voteAverage = cursor.getDouble(5)
                        movie.poster = cursor.getString(6)
                        movie.backdrop = cursor.getString(7)
                        movie.position = cursor.getInt(8)
                        movies.add(movie)
                        cursor.moveToNext()
                    }
                }
                cursor.close()
            }
            return Result.Success(movies)
        } catch (e: Exception) {
            return Result.Error(e)
        }
    }

    override suspend fun updateMovies(movies: List<Movie>) {
        val db = dbHelper.writableDatabase

        val sql = "update " + SQLiteHelper.TABLE_MOVIES + " " +
                "set " + SQLiteHelper.TITLE + " = ?, " +
                SQLiteHelper.RELEASE_DATE + " = ?, " +
                SQLiteHelper.OVERVIEW + " = ?, " +
                SQLiteHelper.VOTES + " = ?, " +
                SQLiteHelper.VOTE_AVG + " = ?, " +
                SQLiteHelper.POSTER + " = ?, " +
                SQLiteHelper.BACKDROP + " = ?, " +
                SQLiteHelper.POSITION + " = ? " +
                "where " + SQLiteHelper.MOVIE_ID + " = ?;"

        db.beginTransaction()
        val stmt = db.compileStatement(sql)

        for (movie in movies) {
            stmt.bindString(1, movie.title)
            stmt.bindString(2, movie.releaseDate)
            stmt.bindString(3, movie.overview)
            stmt.bindLong(4, movie.voteCount.toLong())
            stmt.bindDouble(5, movie.voteAverage)
            stmt.bindString(6, movie.poster)
            stmt.bindString(7, movie.backdrop)
            stmt.bindLong(8, movie.position.toLong())
            stmt.bindLong(9, movie.id)

            stmt.execute()
            stmt.clearBindings()
        }

        db.setTransactionSuccessful()
        db.endTransaction()
    }

    override suspend fun deleteMovies() {
        val db = dbHelper.writableDatabase
        db.delete(SQLiteHelper.TABLE_MOVIES, null, null)
    }
}
