package demo.petproject.kotlinmovies.data.local.sqlite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class SQLiteHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_TABLE_MOVIE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // TODO
    }

    companion object {

        // DATABASE INFORMATION
        const val DATABASE_NAME = "kmovies.db"
        private const val DATABASE_VERSION = 1

        // MOVIES TABLE
        const val TABLE_MOVIES = "movie"
        const val MOVIE_ID = "id"
        const val TITLE = "title"
        const val RELEASE_DATE = "release_date"
        const val OVERVIEW = "overview"
        const val VOTES = "votes"
        const val VOTE_AVG = "vote_average"
        const val POSTER = "poster"
        const val BACKDROP = "backdrop"
        const val POSITION = "position"
        val MOVIE_COLUMNS = arrayOf(MOVIE_ID, TITLE, RELEASE_DATE, OVERVIEW, VOTES, VOTE_AVG, POSTER, BACKDROP, POSITION)

        // DATABASE TABLE CREATION
        private const val CREATE_TABLE_MOVIE = ("create table "
                + TABLE_MOVIES + "( "
                + MOVIE_ID + " integer primary key, "
                + TITLE + " text not null, "
                + RELEASE_DATE + " text not null, "
                + OVERVIEW + " integer not null, "
                + VOTES + " integer not null, "
                + VOTE_AVG + " real not null, "
                + POSTER + " text not null, "
                + BACKDROP + " text not null, "
                + POSITION + " integer not null);")
    }
}
