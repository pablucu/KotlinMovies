package demo.petproject.kotlinmovies.data.remote

import demo.petproject.kotlinmovies.data.remote.entities.JsonMovieData
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesService {

    @GET("movie/popular")
    suspend fun getMovies(@Query("api_key") api_key: String): JsonMovieData

}
