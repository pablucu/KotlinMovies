package demo.petproject.kotlinmovies.data.remote

import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.domain.entities.Result

interface RemoteDao {

    suspend fun getMovies(initialPosition: Int) : Result<List<Movie>>
}