package demo.petproject.kotlinmovies.data.remote

import demo.petproject.kotlinmovies.data.remote.entities.JsonMovie
import demo.petproject.kotlinmovies.data.remote.entities.JsonMovieData
import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.exception.NetworkConnectionException

class RemoteDaoImpl(private val moviesService: MoviesService) : RemoteDao {

    override suspend fun getMovies(initialPosition: Int): Result<List<Movie>> {

        val response: JsonMovieData
        try {
            response = moviesService.getMovies(API_KEY)
        } catch (throwable: Throwable) {
            return Result.Error(NetworkConnectionException("Error downloading movies"))
        }

        return Result.Success(JsonMovie.Mapper.toMovies(response.results, initialPosition))
    }

    companion object {
        private const val API_KEY = "193eb93900fce6bb40bbe87f9a5266ff"
    }

}
