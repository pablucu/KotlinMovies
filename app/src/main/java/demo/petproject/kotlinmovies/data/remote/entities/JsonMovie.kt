package demo.petproject.kotlinmovies.data.remote.entities

import demo.petproject.kotlinmovies.domain.entities.Movie
import java.util.ArrayList

data class JsonMovie(
    val adult: Boolean = false,
    val backdrop_path: String = "",
    val genre_ids: List<Int> = listOf(),
    val id: Long = 0,
    val original_language: String = "",
    val original_title: String = "",
    val overview: String = "",
    val popularity: Double = 0.0,
    val poster_path: String = "",
    val release_date: String = "",
    val title: String = "",
    val video: Boolean = false,
    val vote_average: Double = 0.0,
    val vote_count: Int = 0
) {
    object Mapper {

        fun toMovies(jsonMovies: List<JsonMovie>?, initialPosition: Int): List<Movie> {
            val movies = ArrayList<Movie>()
            var position = initialPosition
            if (jsonMovies != null) {
                for (jsonMovie in jsonMovies) {
                    val movie = toMovie(jsonMovie, position)
                    if (movie != null) {
                        movies.add(movie)
                        position++
                    }
                }
            }
            return movies
        }

        internal fun toMovie(jsonMovie: JsonMovie?, position: Int): Movie? {
            if (jsonMovie == null) {
                return null
            }
            val movie = Movie()
            movie.id = jsonMovie.id
            movie.title = jsonMovie.title
            movie.releaseDate = jsonMovie.release_date
            movie.overview = jsonMovie.overview
            movie.voteCount = jsonMovie.vote_count
            movie.voteAverage = jsonMovie.vote_average
            movie.poster = jsonMovie.poster_path
            movie.backdrop = jsonMovie.backdrop_path
            movie.position = position

            return movie
        }
    }
}
