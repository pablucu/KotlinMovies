package demo.petproject.kotlinmovies.data.remote.entities

data class JsonMovieData(
    val page: Int = 0,
    val results: List<JsonMovie> = listOf(),
    val total_pages: Int = 0,
    val total_results: Int = 0
)