package demo.petproject.kotlinmovies.domain.entities

data class Movie(var id: Long = 0,
                 var title: String = "",
                 var releaseDate: String = "",
                 var overview: String = "",
                 var voteCount: Int = 0,
                 var voteAverage: Double = 0.0,
                 var poster: String = "",
                 var backdrop: String = "",
                 var position: Int = 0)
