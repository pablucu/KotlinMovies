package demo.petproject.kotlinmovies.domain.exception

abstract class BaseException : Exception {
    constructor(errorMessage: String) : super(errorMessage)
    constructor(throwable: Throwable) : super(throwable)
    constructor(errorMessage: String, throwable: Throwable) : super(errorMessage, throwable)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }
}
