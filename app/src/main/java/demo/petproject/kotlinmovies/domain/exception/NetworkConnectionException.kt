package demo.petproject.kotlinmovies.domain.exception

class NetworkConnectionException(message: String) : BaseException(message)
