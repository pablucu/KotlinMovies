package demo.petproject.kotlinmovies.domain.exception

class NoMovieException(message: String) : BaseException(message)
