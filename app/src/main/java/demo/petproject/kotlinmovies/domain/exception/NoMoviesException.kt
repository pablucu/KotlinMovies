package demo.petproject.kotlinmovies.domain.exception

class NoMoviesException(message: String) : BaseException(message)
