package demo.petproject.kotlinmovies.domain.gateways

enum class Message(val code: Int) {

    // Info messages
    LOADING_MOVIES(100),

    // Error messages
    ERROR_LOADING_MOVIES(200),
    ERROR_MOVIE_NOT_AVAILABLE(201)
}