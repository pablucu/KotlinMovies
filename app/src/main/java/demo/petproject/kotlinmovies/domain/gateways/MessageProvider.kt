package demo.petproject.kotlinmovies.domain.gateways

interface MessageProvider {
    fun getMessage(message: Message): String
}