package demo.petproject.kotlinmovies.domain.usecase

import demo.petproject.kotlinmovies.domain.entities.Movie

interface GetLocalMovieUseCase : UseCase<Long, Movie>
