package demo.petproject.kotlinmovies.domain.usecase

import demo.petproject.kotlinmovies.data.local.LocalDao
import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.domain.entities.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetLocalMovieUseCaseImpl(private val localDao: LocalDao,
                               private val dispatcher: CoroutineDispatcher = Dispatchers.IO) : GetLocalMovieUseCase {

    override suspend fun execute(movieId: Long): Result<Movie> = withContext(dispatcher) {
        localDao.getMovie(movieId)
    }

}
