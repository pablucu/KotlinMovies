package demo.petproject.kotlinmovies.domain.usecase

import demo.petproject.kotlinmovies.domain.entities.Movie

interface GetLocalMoviesUseCase : UseCase<Unit, List<Movie>>
