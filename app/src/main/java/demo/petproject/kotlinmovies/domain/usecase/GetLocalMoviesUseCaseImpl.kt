package demo.petproject.kotlinmovies.domain.usecase

import demo.petproject.kotlinmovies.data.local.LocalDao
import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.exception.NoMoviesException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetLocalMoviesUseCaseImpl(private val localDao: LocalDao,
                                private val dispatcher: CoroutineDispatcher = Dispatchers.IO) : GetLocalMoviesUseCase {

    override suspend fun execute(params: Unit): Result<List<Movie>> = withContext(dispatcher) {
        localDao.getMovies().let { result ->
            if (result is Result.Success<List<Movie>> && result.data.isEmpty()) {
                return@withContext Result.Error(NoMoviesException("No movies in the database"))
            }
            return@withContext result
        }
    }

}
