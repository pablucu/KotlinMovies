package demo.petproject.kotlinmovies.domain.usecase

import demo.petproject.kotlinmovies.domain.entities.Movie

interface GetRemoteMoviesUseCase : UseCase<Int, List<Movie>>
