package demo.petproject.kotlinmovies.domain.usecase

import demo.petproject.kotlinmovies.data.remote.RemoteDao
import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.exception.NoMoviesException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetRemoteMoviesUseCaseImpl(private val remoteDao: RemoteDao,
                                 private val dispatcher: CoroutineDispatcher = Dispatchers.IO) : GetRemoteMoviesUseCase {

    override suspend fun execute(initialPosition: Int): Result<List<Movie>> = withContext(dispatcher) {
        remoteDao.getMovies(initialPosition).let { result ->
            if (result is Result.Success<List<Movie>> && result.data.isEmpty()) {
                return@withContext Result.Error(NoMoviesException("No movies in the remote server"))
            }
            return@withContext result
        }
    }

}
