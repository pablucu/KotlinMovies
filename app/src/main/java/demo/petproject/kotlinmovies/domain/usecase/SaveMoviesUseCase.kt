package demo.petproject.kotlinmovies.domain.usecase

import demo.petproject.kotlinmovies.domain.entities.Movie

interface SaveMoviesUseCase : UseCase<List<Movie>, Unit>
