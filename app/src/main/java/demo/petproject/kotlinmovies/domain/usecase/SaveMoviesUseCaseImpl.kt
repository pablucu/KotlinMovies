package demo.petproject.kotlinmovies.domain.usecase

import demo.petproject.kotlinmovies.data.local.LocalDao
import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.domain.entities.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SaveMoviesUseCaseImpl(private val localDao: LocalDao,
                            private val dispatcher: CoroutineDispatcher = Dispatchers.IO) : SaveMoviesUseCase {

    override suspend fun execute(params: List<Movie>): Result<Unit> = withContext(dispatcher) {
        try {
            localDao.deleteMovies()
            localDao.saveMovies(params)
            return@withContext Result.Success(Unit)
        } catch (e: Exception) {
            return@withContext Result.Error(e)
        }
    }
}
