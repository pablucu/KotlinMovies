package demo.petproject.kotlinmovies.domain.usecase

import demo.petproject.kotlinmovies.domain.entities.Result

interface UseCase<in Params, out ResultType> {

    suspend fun execute(params: Params): Result<ResultType>

}
