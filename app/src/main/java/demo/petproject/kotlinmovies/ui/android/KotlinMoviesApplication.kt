package demo.petproject.kotlinmovies.ui.android

import android.app.Application
import demo.petproject.kotlinmovies.di.Factory

class KotlinMoviesApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Factory.init(applicationContext)
    }

}