package demo.petproject.kotlinmovies.ui.android.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import demo.petproject.kotlinmovies.R
import demo.petproject.kotlinmovies.di.Factory
import demo.petproject.kotlinmovies.ui.android.fragment.MoviesFragment
import demo.petproject.kotlinmovies.ui.presentation.MoviesContract
import demo.petproject.kotlinmovies.ui.presentation.MoviesPresenter

class MainActivity : AppCompatActivity() {

    private lateinit var presenter: MoviesContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MoviesPresenter(
            setupMoviesFragment(),
            Factory.getRemoteMoviesUseCase(),
            Factory.getSaveMoviesUseCase(),
            Factory.getLocalMoviesUseCase(),
            Factory.getMessageProvider()
        )
    }

    private fun setupMoviesFragment(): MoviesContract.View {

        var moviesFragment: MoviesFragment? = supportFragmentManager.findFragmentById(R.id.contentFrame) as MoviesFragment?

        if (moviesFragment == null) {
            moviesFragment = MoviesFragment.newInstance()

            // Add fragment to activity
            supportFragmentManager.beginTransaction().add(R.id.contentFrame, moviesFragment).commit()
        }

        return moviesFragment
    }
}
