
package demo.petproject.kotlinmovies.ui.android.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import demo.petproject.kotlinmovies.R
import demo.petproject.kotlinmovies.di.Factory
import demo.petproject.kotlinmovies.ui.android.fragment.MovieDetailsFragment
import demo.petproject.kotlinmovies.ui.presentation.MovieDetailsContract
import demo.petproject.kotlinmovies.ui.presentation.MoviesDetailsPresenter

class MovieDetailsActivity : AppCompatActivity() {

    companion object {
        const val MOVIE_ID_EXTRA = "MOVIE_ID_EXTRA"
    }

    private lateinit var presenter: MovieDetailsContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupActionBar()

        presenter = MoviesDetailsPresenter(setupMovieDetailsFragment(),
                Factory.getLocalMovieUseCase(),
                Factory.getMessageProvider()
        )
    }

    private fun setupActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.movie_details_title)
    }

    private fun setupMovieDetailsFragment(): MovieDetailsContract.View {

        var movieDetailsFragment: MovieDetailsFragment? = supportFragmentManager.findFragmentById(R.id.contentFrame) as MovieDetailsFragment?

        if (movieDetailsFragment == null) {
            movieDetailsFragment = MovieDetailsFragment.newInstance(intent.getLongExtra(MOVIE_ID_EXTRA, 0L))

            // Add fragment to activity
            supportFragmentManager.beginTransaction().add(R.id.contentFrame, movieDetailsFragment).commit()
        }

        return movieDetailsFragment
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
