package demo.petproject.kotlinmovies.ui.android.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import demo.petproject.kotlinmovies.Constants
import demo.petproject.kotlinmovies.R
import demo.petproject.kotlinmovies.domain.entities.Movie


class MovieAdapter(private val context: Context, private val listener: OnItemClickListener) : ListAdapter<Movie, MovieAdapter.ViewHolder>(MovieDiffUtilCallback()) {

    interface OnItemClickListener {
        fun onItemClick(movie: Movie)
    }

    class ViewHolder private constructor(root: View) : RecyclerView.ViewHolder(root) {

        private var container: View = root.findViewById(R.id.adapter_movie_list_container)
        var nameTextView: TextView = root.findViewById(R.id.adapter_movie_list_name)
        var dateTextView: TextView = root.findViewById(R.id.adapter_movie_list_date)
        var ratingTextView: TextView = root.findViewById(R.id.adapter_movie_list_rating_text)
        var posterImageView: ImageView = root.findViewById(R.id.adapter_movie_poster)

        fun bind(movie: Movie, listener: OnItemClickListener) {
            container.setOnClickListener { listener.onItemClick(movie) }
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val v = LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.adapter_movie_list, parent, false)
                return ViewHolder(v)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = getItem(position)

        holder.nameTextView.text = item.title
        holder.dateTextView.text = item.releaseDate
        holder.ratingTextView.text = item.voteAverage.toString()

        Glide.with(context)
            .load(Constants.BASE_IMG_URL + item.poster)
            .transform(RoundedCorners(Constants.ROUNDING_RADIUS))
            .into(holder.posterImageView)

        holder.bind(item, listener)
    }

    override fun getItemCount(): Int {
        return currentList.size
    }
}
