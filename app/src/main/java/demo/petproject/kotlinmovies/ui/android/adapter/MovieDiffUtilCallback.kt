package demo.petproject.kotlinmovies.ui.android.adapter

import androidx.recyclerview.widget.DiffUtil
import demo.petproject.kotlinmovies.domain.entities.Movie

class MovieDiffUtilCallback : DiffUtil.ItemCallback<Movie>() {

    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }
}
