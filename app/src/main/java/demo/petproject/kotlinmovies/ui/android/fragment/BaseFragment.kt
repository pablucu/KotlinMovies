package demo.petproject.kotlinmovies.ui.android.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import demo.petproject.kotlinmovies.R
import demo.petproject.kotlinmovies.ui.presentation.BaseView

abstract class BaseFragment : Fragment(), BaseView {

    private lateinit var messageText: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(getLayout(), container, false)
        initializeUI(root)
        initializeMessageScreen(root)
        return root
    }

    abstract fun getLayout(): Int

    abstract fun initializeUI(root: View)

    private fun initializeMessageScreen(root: View) {
        messageText = root.findViewById(R.id.error_text)
    }

    override fun showMessage(message: String) {
        messageText.visibility = View.VISIBLE
        messageText.text = message
    }

    override fun hideMessage() {
        messageText.visibility = View.GONE
    }
}
