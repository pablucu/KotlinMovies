package demo.petproject.kotlinmovies.ui.android.fragment

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import demo.petproject.kotlinmovies.Constants
import demo.petproject.kotlinmovies.R
import demo.petproject.kotlinmovies.ui.entities.MovieDetailsView
import demo.petproject.kotlinmovies.ui.presentation.MovieDetailsContract

class MovieDetailsFragment : BaseFragment(), MovieDetailsContract.View {

    companion object {
        private const val MOVIE_ID_EXTRA = "MOVIE_ID_EXTRA"

        fun newInstance(movieId: Long): MovieDetailsFragment {
            val movieDetailsFragment = MovieDetailsFragment()
            val arguments = Bundle()
            arguments.putLong(MOVIE_ID_EXTRA, movieId)
            movieDetailsFragment.arguments = arguments
            return movieDetailsFragment
        }
    }

    private lateinit var title: TextView
    private lateinit var date: TextView
    private lateinit var voteAverage: TextView
    private lateinit var voteCount: TextView
    private lateinit var overview: TextView
    private lateinit var backdrop: ImageView

    private lateinit var errorText: TextView

    private lateinit var presenter: MovieDetailsContract.Presenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val movieId = arguments?.getLong(MOVIE_ID_EXTRA) as Long
        presenter.start(movieId)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onCleared()
    }

    override fun getLayout(): Int {
        return R.layout.fragment_movie_details
    }

    override fun initializeUI(root: View) {
        title = root.findViewById(R.id.movie_title)
        date = root.findViewById(R.id.movie_date)
        voteAverage = root.findViewById(R.id.movie_vote_average)
        voteCount = root.findViewById(R.id.movie_vote_count)
        overview = root.findViewById(R.id.movie_overview)
        backdrop = root.findViewById(R.id.movie_poster)
        errorText = root.findViewById(R.id.error_text)
    }

    override fun setPresenter(presenter: MovieDetailsContract.Presenter) {
        this.presenter = presenter
    }

    override fun showMovie(movie: MovieDetailsView) {
        setMovieDetailsVisibility(View.VISIBLE)
        title.text = movie.title
        date.text = movie.releaseDate
        voteAverage.text = movie.voteAverage.toString()
        voteCount.text = getString(R.string.movie_votes, movie.voteCount)
        overview.text = movie.overview
        Glide.with(this)
                .load(Constants.BASE_BACKDROP_URL + movie.backdrop)
                .error(R.drawable.ic_launcher_foreground)
                .centerCrop()
                .into(backdrop)
    }

    override fun showMessage(message: String) {
        super.showMessage(message)
        setMovieDetailsVisibility(View.GONE)
    }

    private fun setMovieDetailsVisibility(visibility: Int) {
        title.visibility = visibility
        date.visibility = visibility
        voteAverage.visibility = visibility
        voteCount.visibility = visibility
        overview.visibility = visibility
        backdrop.visibility = visibility
    }
}
