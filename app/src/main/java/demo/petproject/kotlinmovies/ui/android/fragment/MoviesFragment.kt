package demo.petproject.kotlinmovies.ui.android.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import demo.petproject.kotlinmovies.R
import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.ui.android.adapter.MovieAdapter
import demo.petproject.kotlinmovies.ui.android.activity.MovieDetailsActivity
import demo.petproject.kotlinmovies.ui.presentation.MoviesContract


class MoviesFragment : BaseFragment(), MoviesContract.View, MovieAdapter.OnItemClickListener {

    private lateinit var presenter: MoviesContract.Presenter

    private lateinit var recyclerView: RecyclerView
    private lateinit var movieAdapter: MovieAdapter

    companion object {

        fun newInstance(): MoviesFragment {
            return MoviesFragment()
        }
    }

    override fun getLayout(): Int {
        return R.layout.fragment_movies_list
    }

    override fun initializeUI(root: View) {
        // Title
        activity?.setTitle(R.string.movie_popular_title)

        // Setup RecyclerView
        recyclerView = root.findViewById(R.id.movie_list)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        movieAdapter = MovieAdapter(requireContext(), this)
        recyclerView.adapter = movieAdapter
        recyclerView.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onCleared()
    }

    override fun setPresenter(presenter: MoviesContract.Presenter) {
        this.presenter = presenter
    }

    override fun showLoading(loadingMessage: String) {
        showMessage(loadingMessage)
    }

    override fun showMovies(movies: List<Movie>) {
        recyclerView.visibility = View.VISIBLE
        movieAdapter.submitList(movies)
        hideMessage()
    }

    override fun showMovieDetails(movie: Movie) {
        val intent = Intent(context, MovieDetailsActivity::class.java)
        intent.putExtra(MovieDetailsActivity.MOVIE_ID_EXTRA, movie.id)
        startActivity(intent)
    }

    override fun onItemClick(movie: Movie) {
        presenter.onMovieSelected(movie)
    }
}
