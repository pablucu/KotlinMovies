package demo.petproject.kotlinmovies.ui.entities

import demo.petproject.kotlinmovies.domain.entities.Movie

data class MovieDetailsView(val id: Long = 0,
                            val title: String = "",
                            val releaseDate: String = "",
                            val overview: String = "",
                            val voteCount: Int = 0,
                            val voteAverage: Double = 0.0,
                            val backdrop: String = "") {
    companion object {
        fun map(movie: Movie): MovieDetailsView {
            return MovieDetailsView(movie.id, movie.title, movie.releaseDate, movie.overview,
                    movie.voteCount, movie.voteAverage, movie.backdrop)
        }
    }
}

