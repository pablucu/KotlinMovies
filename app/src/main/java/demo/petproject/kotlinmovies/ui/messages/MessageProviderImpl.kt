package demo.petproject.kotlinmovies.ui.messages

import android.content.Context
import demo.petproject.kotlinmovies.R
import demo.petproject.kotlinmovies.domain.gateways.Message
import demo.petproject.kotlinmovies.domain.gateways.Message.*
import demo.petproject.kotlinmovies.domain.gateways.MessageProvider

class MessageProviderImpl(private val context: Context): MessageProvider {

    override fun getMessage(message: Message): String {
        return when (message) {
            LOADING_MOVIES -> {
                context.getString(R.string.movie_list_loading)
            }
            ERROR_LOADING_MOVIES -> {
                context.getString(R.string.error_loading_movies)
            }
            ERROR_MOVIE_NOT_AVAILABLE -> {
                context.getString(R.string.error_no_movie)
            }
        }
    }
}
