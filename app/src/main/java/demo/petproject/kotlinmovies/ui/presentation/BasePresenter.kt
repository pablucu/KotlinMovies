package demo.petproject.kotlinmovies.ui.presentation

interface BasePresenter {

    fun onCleared()

}