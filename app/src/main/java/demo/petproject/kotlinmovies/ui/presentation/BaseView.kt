package demo.petproject.kotlinmovies.ui.presentation

interface BaseView {

    fun showMessage(message: String)

    fun hideMessage()

}
