package demo.petproject.kotlinmovies.ui.presentation

import demo.petproject.kotlinmovies.ui.entities.MovieDetailsView

interface MovieDetailsContract {
    interface View : BaseView {
        fun setPresenter(presenter: Presenter)

        fun showMovie(movie: MovieDetailsView)
    }

    interface Presenter : BasePresenter {
        fun start(movieId: Long)
    }
}
