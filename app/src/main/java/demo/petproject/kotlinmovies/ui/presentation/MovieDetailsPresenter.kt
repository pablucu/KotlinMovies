package demo.petproject.kotlinmovies.ui.presentation

import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.gateways.Message
import demo.petproject.kotlinmovies.domain.gateways.MessageProvider
import demo.petproject.kotlinmovies.domain.usecase.GetLocalMovieUseCase
import demo.petproject.kotlinmovies.ui.entities.MovieDetailsView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MoviesDetailsPresenter(private val view: MovieDetailsContract.View,
                             private val getLocalMovieUseCase: GetLocalMovieUseCase,
                             private val messageProvider: MessageProvider) : MovieDetailsContract.Presenter {

    private val job = Job()
    private val customScope = CoroutineScope(Dispatchers.Main + job)

    init {
        view.setPresenter(this)
    }

    override fun start(movieId: Long) {
        customScope.launch {
            when (val result = getLocalMovieUseCase.execute(movieId)) {
                is Result.Success<Movie> -> {
                    view.showMovie(MovieDetailsView.map(result.data))
                }
                is Result.Error -> {
                    view.showMessage(messageProvider.getMessage(Message.ERROR_MOVIE_NOT_AVAILABLE))
                }
            }
        }
    }

    override fun onCleared() {
        job.cancel()
    }

}
