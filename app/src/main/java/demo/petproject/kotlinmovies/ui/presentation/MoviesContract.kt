package demo.petproject.kotlinmovies.ui.presentation

import demo.petproject.kotlinmovies.domain.entities.Movie

interface MoviesContract {
    interface View : BaseView {
        fun setPresenter(presenter: Presenter)

        fun showLoading(loadingMessage: String)

        fun showMovies(movies: List<Movie>)

        fun showMovieDetails(movie: Movie)
    }

    interface Presenter : BasePresenter {
        fun start()

        fun onMovieSelected(movie: Movie)
    }
}