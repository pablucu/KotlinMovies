package demo.petproject.kotlinmovies.ui.presentation

import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.gateways.Message
import demo.petproject.kotlinmovies.domain.gateways.MessageProvider
import demo.petproject.kotlinmovies.domain.usecase.GetLocalMoviesUseCase
import demo.petproject.kotlinmovies.domain.usecase.GetRemoteMoviesUseCase
import demo.petproject.kotlinmovies.domain.usecase.SaveMoviesUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MoviesPresenter(private val view: MoviesContract.View,
                      private val getRemoteMoviesUseCase: GetRemoteMoviesUseCase,
                      private val saveMoviesUseCase: SaveMoviesUseCase,
                      private val getLocalMoviesUseCase: GetLocalMoviesUseCase,
                      private val messageProvider: MessageProvider) : MoviesContract.Presenter {

    private val job = Job()
    private val customScope = CoroutineScope(Dispatchers.Main + job)

    init {
        view.setPresenter(this)
    }

    override fun start() {
        view.showLoading(messageProvider.getMessage(Message.LOADING_MOVIES))
        customScope.launch {
            fetchData()
        }
    }

    private suspend fun fetchData() {
        getRemoteMoviesUseCase.execute(1).let { result ->
            when (result) {
                is Result.Success<List<Movie>> -> {
                    // Show downloaded movies
                    view.showMovies(result.data)
                    // Save downloaded movies
                    saveMoviesUseCase.execute(result.data)
                }
                is Result.Error -> {
                    displayLocalMovies()
                }
            }
        }
    }

    private suspend fun displayLocalMovies() {
        getLocalMoviesUseCase.execute(Unit).let { result ->
            when (result) {
                is Result.Success<List<Movie>> -> {
                    view.showMovies(result.data)
                }
                is Result.Error -> {
                    view.showMessage(messageProvider.getMessage(Message.ERROR_LOADING_MOVIES))
                }
            }
        }
    }

    override fun onMovieSelected(movie: Movie) {
        view.showMovieDetails(movie)
    }

    override fun onCleared() {
        job.cancel()
    }
}
