package demo.petproject.kotlinmovies.di

import android.content.Context
import demo.petproject.kotlinmovies.data.local.LocalDao
import demo.petproject.kotlinmovies.data.remote.RemoteDao
import demo.petproject.kotlinmovies.domain.gateways.MessageProvider
import demo.petproject.kotlinmovies.domain.usecase.*
import demo.petproject.kotlinmovies.ui.messages.MessageProviderImpl
import org.mockito.Mockito

class Factory {

    companion object {

        private lateinit var appContext: Context

        private lateinit var localDaoInstance: LocalDao
        private lateinit var remoteDaoInstance: RemoteDao
        private lateinit var messageProvider: MessageProvider

        fun init(appContext: Context) {
            this.appContext = appContext
        }

        fun getLocalDao(): LocalDao {
            if (!this::localDaoInstance.isInitialized) {
                synchronized(this) {
                    localDaoInstance = Mockito.mock(LocalDao::class.java)
                }
            }
            return localDaoInstance
        }

        fun getRemoteDao(): RemoteDao {
            if (!this::remoteDaoInstance.isInitialized) {
                synchronized(this) {
                    remoteDaoInstance = Mockito.mock(RemoteDao::class.java)
                }
            }
            return remoteDaoInstance
        }

        fun getMessageProvider(): MessageProvider {
            if (!this::messageProvider.isInitialized) {
                synchronized(this) {
                    messageProvider = MessageProviderImpl(appContext)
                }
            }
            return messageProvider
        }

        // Use cases

        fun getLocalMoviesUseCase(): GetLocalMoviesUseCase {
            return GetLocalMoviesUseCaseImpl(getLocalDao())
        }

        fun getLocalMovieUseCase(): GetLocalMovieUseCase {
            return GetLocalMovieUseCaseImpl(getLocalDao())
        }

        fun getSaveMoviesUseCase(): SaveMoviesUseCase {
            return SaveMoviesUseCaseImpl(getLocalDao())
        }

        fun getRemoteMoviesUseCase(): GetRemoteMoviesUseCase {
            return GetRemoteMoviesUseCaseImpl(getRemoteDao())
        }
    }
}
