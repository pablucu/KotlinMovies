package demo.petproject.kotlinmovies.di

import android.content.Context
import demo.petproject.kotlinmovies.BuildConfig
import demo.petproject.kotlinmovies.Constants
import demo.petproject.kotlinmovies.data.local.LocalDao
import demo.petproject.kotlinmovies.data.local.sqlite.LocalDaoImpl
import demo.petproject.kotlinmovies.data.remote.MoviesService
import demo.petproject.kotlinmovies.data.remote.RemoteDao
import demo.petproject.kotlinmovies.data.remote.RemoteDaoImpl
import demo.petproject.kotlinmovies.domain.gateways.MessageProvider
import demo.petproject.kotlinmovies.domain.usecase.*
import demo.petproject.kotlinmovies.ui.messages.MessageProviderImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Factory {

    companion object {

        private lateinit var appContext: Context

        private lateinit var localDaoInstance: LocalDao
        private lateinit var remoteDaoInstance: RemoteDao
        private lateinit var messageProvider: MessageProvider

        fun init(appContext: Context) {
            this.appContext = appContext
        }

        fun getLocalDao(): LocalDao {
            if (!this::localDaoInstance.isInitialized) {
                synchronized(this) {
                    localDaoInstance = LocalDaoImpl(appContext)
                }
            }
            return localDaoInstance
        }

        fun getRemoteDao(): RemoteDao {
            if (!this::remoteDaoInstance.isInitialized) {
                synchronized(this) {
                    remoteDaoInstance = RemoteDaoImpl(getMoviesService())
                }
            }
            return remoteDaoInstance
        }

        private fun getMoviesService(): MoviesService {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

            return Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MoviesService::class.java)
        }

        fun getMessageProvider(): MessageProvider {
            if (!this::messageProvider.isInitialized) {
                synchronized(this) {
                    messageProvider = MessageProviderImpl(appContext)
                }
            }
            return messageProvider
        }

        // Use cases

        fun getLocalMoviesUseCase(): GetLocalMoviesUseCase {
            return GetLocalMoviesUseCaseImpl(getLocalDao())
        }

        fun getLocalMovieUseCase(): GetLocalMovieUseCase {
            return GetLocalMovieUseCaseImpl(getLocalDao())
        }

        fun getSaveMoviesUseCase(): SaveMoviesUseCase {
            return SaveMoviesUseCaseImpl(getLocalDao())
        }

        fun getRemoteMoviesUseCase(): GetRemoteMoviesUseCase {
            return GetRemoteMoviesUseCaseImpl(getRemoteDao())
        }
    }
}
