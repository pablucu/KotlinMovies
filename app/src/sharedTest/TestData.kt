package demo.petproject.kotlinmovies

import demo.petproject.kotlinmovies.domain.entities.Movie

class TestData {
    companion object {
        val MOVIE_1 = Movie(
            1L,
            "Movie One",
            "30/03/1995",
            "1st overview",
            1000,
            5.5,
            "posterUrl1",
            "backdropUrl1",
            1
        )
        val MOVIE_2 = Movie(
            2L,
            "Movie Two",
            "28/02/1950",
            "2nd overview",
            2000,
            7.5,
            "posterUrl2",
            "backdropUrl2",
            2
        )
    }
}
