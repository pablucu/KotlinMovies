package demo.petproject.kotlinmovies.data.remote

import java.nio.charset.StandardCharsets

class JsonReader {

    companion object {

        fun read(jsonFile: String): String {
            val inputStream = ClassLoader.getSystemResourceAsStream(jsonFile)
            val jsonString = String(inputStream.readBytes(), StandardCharsets.UTF_8)
            inputStream.close()
            return jsonString
        }
    }
}