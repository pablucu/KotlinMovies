package demo.petproject.kotlinmovies.data.remote

import demo.petproject.kotlinmovies.domain.entities.Movie
import demo.petproject.kotlinmovies.domain.entities.Result
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class RemoteDaoImplTest {

    private lateinit var remoteDao: RemoteDaoImpl

    private lateinit var server: MockWebServer

    @Before
    fun setUp() {
        server = MockWebServer()
        server.start(8080)

        remoteDao = RemoteDaoImpl(
            Retrofit.Builder()
                .baseUrl(server.url("/"))
                .client(OkHttpClient.Builder().build())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MoviesService::class.java)
        )
    }

    @After
    fun tearDown() {
        server.shutdown()
    }

    @Test
    fun test_getMovies_valid() = runBlocking {

        // Setup valid response
        server.enqueue(MockResponse().setResponseCode(200).setBody(JsonReader.read("movies.json")))

        val movies = remoteDao.getMovies(1) as Result.Success<List<Movie>>

        Assert.assertTrue(movies.data.isNotEmpty())
        Assert.assertEquals(20, movies.data.size)
    }

    @Test
    fun test_getMovies_error() = runBlocking {

        // Setup not found response
        server.enqueue(MockResponse().setResponseCode(404).setBody(""))

        val movies = remoteDao.getMovies(1)

        Assert.assertTrue(movies is Result.Error)
    }

}
