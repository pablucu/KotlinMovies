package demo.petproject.kotlinmovies.data.remote.entities

import demo.petproject.kotlinmovies.TestData
import org.junit.Assert
import org.junit.Test

class JsonMovieMapperTest {

    @Test
    fun toMovie_nullJsonMovie() {
        val movie = JsonMovie.Mapper.toMovie(null, 1)

        // Check mapped movie
        Assert.assertNull(movie)
    }

    @Test
    fun toMovie_defaultJsonMovie() {
        val movie = JsonMovie.Mapper.toMovie(JsonMovie(), 1)

        // Check mapped movie
        Assert.assertNotNull(movie!!)
        Assert.assertEquals(0, movie.id)
        Assert.assertEquals("", movie.title)
        Assert.assertEquals("", movie.releaseDate)
        Assert.assertEquals("", movie.overview)
        Assert.assertEquals(0, movie.voteCount)
        Assert.assertEquals(0.0, movie.voteAverage, 0.0)
        Assert.assertEquals("", movie.poster)
        Assert.assertEquals("", movie.backdrop)
        Assert.assertEquals(1, movie.position)
    }

    @Test
    fun toMovie_mapMovie2() {
        val jsonMovie = JsonMovie(
            id = 2L,
            title = "Movie Two",
            release_date = "28/02/1950",
            overview = "2nd overview",
            vote_count = 2000,
            vote_average = 7.5,
            poster_path = "posterUrl2",
            backdrop_path = "backdropUrl2"
        )

        val movie = JsonMovie.Mapper.toMovie(jsonMovie, 2)

        // Check mapped movie
        Assert.assertNotNull(movie!!)
        Assert.assertEquals(TestData.MOVIE_2, movie)
    }

    @Test
    fun toMovies_nullList() {
        val movies = JsonMovie.Mapper.toMovies(null, 1)
        // Check mapped movies
        Assert.assertEquals(0, movies.size)
    }

    @Test
    fun toMovies_emptyList() {
        val movies = JsonMovie.Mapper.toMovies(listOf(), 1)
        // Check mapped movies
        Assert.assertEquals(0, movies.size)
    }

    @Test
    fun toMovie_mapMovieList() {
        val jsonMovie1 = JsonMovie(
            id = 1L,
            title = "Movie One",
            release_date = "30/03/1995",
            overview = "1st overview",
            vote_count = 1000,
            vote_average = 5.5,
            poster_path = "posterUrl1",
            backdrop_path = "backdropUrl1"
        )

        val jsonMovie2 = JsonMovie(
            id = 2L,
            title = "Movie Two",
            release_date = "28/02/1950",
            overview = "2nd overview",
            vote_count = 2000,
            vote_average = 7.5,
            poster_path = "posterUrl2",
            backdrop_path = "backdropUrl2"
        )

        val movies = JsonMovie.Mapper.toMovies(listOf(jsonMovie1, jsonMovie2), 1)

        // Check mapped movies
        Assert.assertEquals(2, movies.size)
        Assert.assertEquals(TestData.MOVIE_1, movies[0])
        Assert.assertEquals(TestData.MOVIE_2, movies[1])
    }

}
