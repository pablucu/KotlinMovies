package demo.petproject.kotlinmovies.domain.usecase

import demo.petproject.kotlinmovies.TestData
import demo.petproject.kotlinmovies.data.local.LocalDao
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.exception.NoMovieException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.sql.SQLException

@ExperimentalCoroutinesApi
class GetLocalMovieUseCaseTest {

    @Mock
    lateinit var localDao: LocalDao

    private lateinit var useCase: GetLocalMovieUseCase

    private val testDispatcher = UnconfinedTestDispatcher()

    @Before
    fun setupMocks() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
        useCase = GetLocalMovieUseCaseImpl(localDao)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun getMovie_success() = runTest {
        // Simulate LocalDao with MOVIE_1
        Mockito.`when`(localDao.getMovie(TestData.MOVIE_1.id))
            .thenReturn(Result.Success(TestData.MOVIE_1))

        // When calling the use case
        val result = useCase.execute(TestData.MOVIE_1.id)

        // Verify the result is a success and contains the expected movie
        Assert.assertTrue(result is Result.Success)
        Assert.assertEquals(TestData.MOVIE_1, (result as Result.Success).data)
    }

    @Test
    fun getMovie_notFound() = runTest {
        // Simulate LocalDao without movie with id 999
        Mockito.`when`(localDao.getMovie(999L))
            .thenReturn(Result.Error(NoMovieException("Movie not found!")))

        // When calling the use case
        val result = useCase.execute(999L)

        // Verify the result is an error
        Assert.assertTrue(result is Result.Error)
        Assert.assertTrue((result as Result.Error).exception is NoMovieException)
        Assert.assertEquals("Movie not found!", result.exception.message)
    }

    @Test
    fun getMovie_error() = runTest {
        // Simulate LocalDao error
        Mockito.`when`(localDao.getMovie(999L))
            .thenReturn(Result.Error(SQLException("Wrong query.")))

        // When calling the use case
        val result = useCase.execute(999L)

        // Verify the result is an error
        Assert.assertTrue(result is Result.Error)
        Assert.assertTrue((result as Result.Error).exception is SQLException)
    }

}
