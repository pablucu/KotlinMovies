package demo.petproject.kotlinmovies.domain.usecase

import demo.petproject.kotlinmovies.TestData
import demo.petproject.kotlinmovies.data.local.LocalDao
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.exception.NoMoviesException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.sql.SQLException

@ExperimentalCoroutinesApi
class GetLocalMoviesUseCaseTest {

    @Mock
    lateinit var localDao: LocalDao

    private lateinit var useCase: GetLocalMoviesUseCase

    private val testDispatcher = UnconfinedTestDispatcher()

    @Before
    fun setupMocks() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
        useCase = GetLocalMoviesUseCaseImpl(localDao)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun getMovies_all() = runTest {
        // Simulate LocalDao with two movies
        Mockito.`when`(localDao.getMovies())
            .thenReturn(Result.Success(listOf(TestData.MOVIE_1, TestData.MOVIE_2)))

        // When calling the use case
        val result = useCase.execute(Unit)

        // Verify the result is a success and contains two movies
        Assert.assertTrue(result is Result.Success)
        Assert.assertEquals((result as Result.Success).data.size, 2)
        Assert.assertEquals(TestData.MOVIE_1, result.data[0])
        Assert.assertEquals(TestData.MOVIE_2, result.data[1])
    }

    @Test
    fun getMovies_localDaoError() = runTest {
        // Simulate LocalDao error
        Mockito.`when`(localDao.getMovies()).thenReturn(Result.Error(SQLException("Wrong query.")))

        // When calling the use case
        val result = useCase.execute(Unit)

        // Verify the result is an error
        Assert.assertTrue(result is Result.Error)
        Assert.assertTrue((result as Result.Error).exception is SQLException)
    }

    @Test
    fun getMovies_emptyLocalDao() = runTest {
        // Simulate empty LocalDao
        Mockito.`when`(localDao.getMovies()).thenReturn(Result.Success(listOf()))

        // When calling the use case
        val result = useCase.execute(Unit)

        // Verify the result is an error
        Assert.assertTrue(result is Result.Error)
        Assert.assertTrue((result as Result.Error).exception is NoMoviesException)
        Assert.assertEquals("No movies in the database", result.exception.message)
    }

}
