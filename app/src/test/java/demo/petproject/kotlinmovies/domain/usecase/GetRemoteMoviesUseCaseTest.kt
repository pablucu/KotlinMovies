package demo.petproject.kotlinmovies.domain.usecase

import demo.petproject.kotlinmovies.TestData
import demo.petproject.kotlinmovies.data.remote.RemoteDao
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.exception.NetworkConnectionException
import demo.petproject.kotlinmovies.domain.exception.NoMoviesException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class GetRemoteMoviesUseCaseTest {

    @Mock
    lateinit var remoteDao: RemoteDao

    private lateinit var useCase: GetRemoteMoviesUseCase

    private val testDispatcher = UnconfinedTestDispatcher()

    @Before
    fun setupMocks() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
        useCase = GetRemoteMoviesUseCaseImpl(remoteDao)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun getMovies_all() = runTest {
        // Simulate RemoteDao with two movies
        Mockito.`when`(remoteDao.getMovies(1))
            .thenReturn(Result.Success(listOf(TestData.MOVIE_1, TestData.MOVIE_2)))

        // When calling the use case
        val result = useCase.execute(1)

        // Verify the result is a success and contains two movies
        Assert.assertTrue(result is Result.Success)
        Assert.assertEquals((result as Result.Success).data.size, 2)
        Assert.assertEquals(TestData.MOVIE_1, result.data[0])
        Assert.assertEquals(TestData.MOVIE_2, result.data[1])
    }

    @Test
    fun getMovies_remoteDaoError() = runTest {
        // Simulate RemoteDao error
        Mockito.`when`(remoteDao.getMovies(1))
            .thenReturn(Result.Error(NetworkConnectionException("Error downloading movies")))

        // When calling the use case
        val result = useCase.execute(1)

        // Verify the result is an error
        Assert.assertTrue(result is Result.Error)
        Assert.assertTrue((result as Result.Error).exception is NetworkConnectionException)
    }

    @Test
    fun getMovies_emptyRemoteDao() = runTest {
        // Simulate empty RemoteDao
        Mockito.`when`(remoteDao.getMovies(1)).thenReturn(Result.Success(listOf()))

        // When calling the use case
        val result = useCase.execute(1)

        // Verify the result is an error
        Assert.assertTrue(result is Result.Error)
        Assert.assertTrue((result as Result.Error).exception is NoMoviesException)
        Assert.assertEquals("No movies in the remote server", result.exception.message)
    }

}
