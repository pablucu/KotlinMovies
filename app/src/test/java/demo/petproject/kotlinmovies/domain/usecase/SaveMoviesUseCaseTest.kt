package demo.petproject.kotlinmovies.domain.usecase

import demo.petproject.kotlinmovies.TestData
import demo.petproject.kotlinmovies.data.local.LocalDao
import demo.petproject.kotlinmovies.domain.entities.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class SaveMoviesUseCaseTest {

    @Mock
    lateinit var localDao: LocalDao

    private lateinit var useCase: SaveMoviesUseCase

    private val testDispatcher = UnconfinedTestDispatcher()

    @Before
    fun setupMocks() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
        useCase = SaveMoviesUseCaseImpl(localDao)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun saveMovies_success() = runTest {
        // When calling the use case
        val result = useCase.execute(listOf(TestData.MOVIE_1, TestData.MOVIE_2))

        // Verify the result is a success
        Assert.assertTrue(result is Result.Success)
    }

    @Test
    fun saveMovies_error() = runTest {
        // Simulate LocalDao error
        Mockito.`when`(localDao.saveMovies(listOf(TestData.MOVIE_1, TestData.MOVIE_2)))
            .thenThrow((RuntimeException("Could not perform operation.")))

        // When calling the use case
        val result = useCase.execute(listOf(TestData.MOVIE_1, TestData.MOVIE_2))

        // Verify the result is an error
        Assert.assertTrue(result is Result.Error)
        Assert.assertTrue((result as Result.Error).exception is RuntimeException)
    }
}
