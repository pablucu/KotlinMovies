package demo.petproject.kotlinmovies.ui.presentation

import demo.petproject.kotlinmovies.TestData
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.exception.NoMovieException
import demo.petproject.kotlinmovies.domain.gateways.Message
import demo.petproject.kotlinmovies.domain.gateways.MessageProvider
import demo.petproject.kotlinmovies.domain.usecase.GetLocalMovieUseCase
import demo.petproject.kotlinmovies.ui.entities.MovieDetailsView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class MovieDetailsPresenterTest {

    @Mock
    internal lateinit var view: MovieDetailsContract.View

    @Mock
    internal lateinit var getLocalMovieUseCase: GetLocalMovieUseCase

    @Mock
    internal lateinit var messageProvider: MessageProvider

    private lateinit var presenter: MoviesDetailsPresenter

    private val testDispatcher = UnconfinedTestDispatcher()

    @Before
    fun setupMocks() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun test_When_PresenterIsCreated_Then_IsAttachedToView() {

        // Class under test
        presenter = MoviesDetailsPresenter(view, getLocalMovieUseCase, messageProvider)

        // Then the presenter is set to the view
        Mockito.verify(view).setPresenter(presenter)
    }

    @Test
    fun test_Given_MovieInLocalData_Then_DisplayMovieDetails() = runTest {

        // Setup GetLocalMovieUseCase with one movie
        Mockito.`when`(getLocalMovieUseCase.execute(TestData.MOVIE_1.id)).thenReturn(Result.Success(TestData.MOVIE_1))

        // Class under test
        presenter = MoviesDetailsPresenter(view, getLocalMovieUseCase, messageProvider)

        // When the presenter is asked to start
        presenter.start(TestData.MOVIE_1.id)

        // Then verify local data is displayed
        val expected = MovieDetailsView(TestData.MOVIE_1.id, TestData.MOVIE_1.title,
                TestData.MOVIE_1.releaseDate, TestData.MOVIE_1.overview,
                TestData.MOVIE_1.voteCount, TestData.MOVIE_1.voteAverage, TestData.MOVIE_1.backdrop)
        Mockito.verify(view).showMovie(expected)
    }

    @Test
    fun test_Given_UnknownMovie_Then_DisplayError() = runTest {

        // Setup GetLocalMovieUseCase without movie
        Mockito.`when`(getLocalMovieUseCase.execute(TestData.MOVIE_1.id)).thenReturn(Result.Error(NoMovieException("Movie not found!")))

        // Setup MessageProvider's message
        setupMessages()

        // Class under test
        presenter = MoviesDetailsPresenter(view, getLocalMovieUseCase, messageProvider)

        // When the presenter is asked to start
        presenter.start(TestData.MOVIE_1.id)

        // Then verify error displayed
        Mockito.verify(view).showMessage(ArgumentMatchers.matches("movie_not_available"))
    }

    private fun setupMessages() {
        Mockito.`when`(messageProvider.getMessage(Message.ERROR_MOVIE_NOT_AVAILABLE)).thenReturn("movie_not_available")
    }
}
