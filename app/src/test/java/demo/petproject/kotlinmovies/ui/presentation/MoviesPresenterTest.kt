package demo.petproject.kotlinmovies.ui.presentation

import demo.petproject.kotlinmovies.TestData.Companion.MOVIE_1
import demo.petproject.kotlinmovies.TestData.Companion.MOVIE_2
import demo.petproject.kotlinmovies.domain.entities.Result
import demo.petproject.kotlinmovies.domain.exception.NetworkConnectionException
import demo.petproject.kotlinmovies.domain.exception.NoMoviesException
import demo.petproject.kotlinmovies.domain.gateways.Message
import demo.petproject.kotlinmovies.domain.gateways.MessageProvider
import demo.petproject.kotlinmovies.domain.usecase.GetLocalMoviesUseCase
import demo.petproject.kotlinmovies.domain.usecase.GetRemoteMoviesUseCase
import demo.petproject.kotlinmovies.domain.usecase.SaveMoviesUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyList
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class MoviesPresenterTest {

    @Mock
    internal lateinit var view: MoviesContract.View

    @Mock
    internal lateinit var getRemoteMoviesUseCase: GetRemoteMoviesUseCase

    @Mock
    internal lateinit var saveMoviesUseCase: SaveMoviesUseCase

    @Mock
    internal lateinit var getLocalMoviesUseCase: GetLocalMoviesUseCase

    @Mock
    internal lateinit var messageProvider: MessageProvider

    private lateinit var presenter: MoviesPresenter

    private val testDispatcher = UnconfinedTestDispatcher()

    @Before
    fun setupMocks() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun test_When_PresenterIsCreated_Then_IsAttachedToView() {

        // Class under test
        presenter = MoviesPresenter(view, getRemoteMoviesUseCase, saveMoviesUseCase, getLocalMoviesUseCase, messageProvider)

        // Then the presenter is set to the view
        verify(view).setPresenter(presenter)
    }

    @Test
    fun test_Given_RemoteData_Then_DisplayRemoteData() = runTest {

        // Simulate GetRemoteMoviesUseCase successful response with sample remote data
        Mockito.`when`(getRemoteMoviesUseCase.execute(1)).thenReturn(Result.Success(listOf(MOVIE_1, MOVIE_2)))

        // Class under test
        presenter = MoviesPresenter(view, getRemoteMoviesUseCase, saveMoviesUseCase, getLocalMoviesUseCase, messageProvider)

        // When the presenter is asked to start/fetch the data
        presenter.start()

        // Then loading view is shown
        verify(view).showLoading(messageProvider.getMessage(Message.LOADING_MOVIES))

        // And it is displayed
        verify(view).showMovies(listOf(MOVIE_1, MOVIE_2))
    }

    @Test
    fun test_Given_NoRemoteData_And_LocalData_Then_DisplayLocalData() = runTest {

        // Setup GetRemoteMoviesUseCase to return an error
        Mockito.`when`(getRemoteMoviesUseCase.execute(1)).thenReturn(Result.Error(NetworkConnectionException("Error downloading movies")))
        // Setup LocalDao with one movie
        Mockito.`when`(getLocalMoviesUseCase.execute(Unit)).thenReturn(Result.Success(listOf(MOVIE_1)))

        // Class under test
        presenter = MoviesPresenter(view, getRemoteMoviesUseCase, saveMoviesUseCase, getLocalMoviesUseCase, messageProvider)

        // When the presenter is asked to start/fetch the data
        presenter.start()

        // Then loading view is shown
        verify(view).showLoading(messageProvider.getMessage(Message.LOADING_MOVIES))

        // And local data is displayed
        verify(view).showMovies(listOf(MOVIE_1))
    }

    @Test
    fun test_Given_NoRemoteData_And_NoLocalData_Then_DisplayError() = runTest {

        // Setup GetRemoteMoviesUseCase to return an error
        Mockito.`when`(getRemoteMoviesUseCase.execute(1)).thenReturn(Result.Error(NetworkConnectionException("Error downloading movies")))
        // Setup GetLocalMoviesUseCase to return an error
        Mockito.`when`(getLocalMoviesUseCase.execute(Unit)).thenReturn(Result.Error(NoMoviesException("No Movies found!")))

        // Class under test
        presenter = MoviesPresenter(view, getRemoteMoviesUseCase, saveMoviesUseCase, getLocalMoviesUseCase, messageProvider)

        // When the presenter is asked to start/fetch the data
        presenter.start()

        // Then loading view is shown
        verify(view).showLoading(messageProvider.getMessage(Message.LOADING_MOVIES))

        // And no movies screen is displayed
        verify(view, times(0)).showMovies(anyList())
    }

    @Test
    fun test_When_MovieIsSelected_Then_DisplayMovieDetails() {

        // Class under test
        presenter = MoviesPresenter(view, getRemoteMoviesUseCase, saveMoviesUseCase, getLocalMoviesUseCase, messageProvider)

        // When a movie is selected
        presenter.onMovieSelected(MOVIE_1)

        // Then the details view is displayed
        verify(view).showMovieDetails(MOVIE_1)
    }
}
